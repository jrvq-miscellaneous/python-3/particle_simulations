# -*- coding: UTF-8 -*-
"""
Author: Jaime Rivera
File: particle_classes.py
Date: 2019.11.29
Revision: 2020.01.12
Copyright: Copyright 2019 Jaime Rivera

           Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
           documentation files (the "Software"), to deal in the Software without restriction, including without
           limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
           the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
           conditions:

           The above copyright notice and this permission notice shall be included in all copies or substantial
           portions of the Software.

           THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
           TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
           SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
           ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
           OR OTHER DEALINGS IN THE SOFTWARE.

Brief:

"""

__author__ = 'Jaime Rivera <www.jaimervq.com>'
__copyright__ = 'Copyright 2019, Jaime Rivera'
__credits__ = ['The "Enjoying Code" group at Ilion Animation Studios']
__license__ = 'MIT License'
__maintainer__ = 'Jaime Rivera'
__email__ = 'jaime.rvq@gmail.com'
__status__ = 'Development'


import random
import math

from collections import deque

import numpy


# ------------- POLYGONS AND COLLISION TYPES ------------- #
TRIANGLE = 'TRIANGLE'
SQUARE = 'SQUARE'
PENTAGON = 'PENTAGON'
STAR = 'STAR'
CIRCLE = 'CIRCLE'

# COLLISION TYPES
NEUTRAL_COLL = 0
REBOUND_COLL = 1
DESTROY_COLL = 2
FUSION_COLL = 3


# ------------- VECTOR2 CLASS ------------- #
class Vector2(object):
    def __init__(self, init_x=0, init_y=0):
        self.x = init_x
        self.y = init_y

    def get_module(self):
        module = math.sqrt(self.x ** 2 + self.y ** 2)
        return module

    def get_normalized(self):
        module = self.get_module()
        if module:
            return Vector2(self.x/module, self.y/module)
        else:
            return Vector2(self.x, self.y)

    def rotate(self, angle=None, clip=True):

        if not angle:
            angle = random.randint(-60, 60)

        x, y = self.x, self.y
        x2 = math.cos(math.radians(angle)) * x - math.sin(math.radians(angle)) * y
        y2 = math.sin(math.radians(angle)) * x + math.cos(math.radians(angle)) * y

        if clip:
            x2, y2 = numpy.clip(x2, -1, 1), numpy.clip(y2, -1, 1)

        self.x, self.y = x2, y2

    def get_xy_tuple(self):
        return (self.x, self.y)

    def get_distance_to_other(self, other):
        x1, y1 = self.x, self.y
        x2, y2 = other.x, other.y
        return math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

    def __add__(self, other):
        return Vector2(self.x + other.x, self.y + other.y, )

    def __rmul__(self, other):
        if isinstance(other, int) or isinstance(other, float):
            return Vector2(other * self.x, other * self.y)

    def __str__(self):
        return "Vector2({:.3f}, {:.3f})".format(self.x, self.y)

    @staticmethod
    def get_bounding_box(vec1, vec2):
        return [(min(vec1.x, vec2.x), min(vec1.y, vec2.y)),
                (max(vec1.x, vec2.x), max(vec1.y, vec2.y))]


# ------------- PARTICLE CLASS ------------- #
class MovingParticle(object):

    def __init__(self, boundaries, initial_pos,
                 particle_radius, internal_rotation_increment,
                 speed_rot_randomisation_factor, min_speed_mult, max_speed_mult,
                 collision_probabilities,
                 poly, polygon_probabilities,
                 paint_trails, min_trail_length, max_trail_length):

        # Radius
        self.radius = particle_radius

        # Boundaries
        self.boundaries_x = boundaries[0] - 1 - particle_radius
        self.boundaries_y = boundaries[1] - 1 - particle_radius

        # Position
        if not initial_pos:
            self.position_vec = Vector2(random.randint(particle_radius, self.boundaries_x),
                                        random.randint(particle_radius, self.boundaries_y))
        else:
            self.position_vec = Vector2(initial_pos[0], initial_pos[1])

        # Speed
        self.speed_vec = Vector2(random.uniform(-1, 1), random.uniform(-1, 1))
        speed_module = numpy.clip(self.speed_vec.get_module(), 0, 1)
        self.speed_rot_randomisation = speed_module * speed_rot_randomisation_factor

        self.speed_mult = random.uniform(min_speed_mult, max_speed_mult)

        # Internal rotation
        self.internal_rotation = random.randint(1, 360)
        self.internal_rotation_increment = random.choice([1, -1]) * random.randint(0, internal_rotation_increment)

        # Collision type
        self.collision_type = random.choices([0, 1, 2, 3], weights=collision_probabilities, k=1)[0]
        
        # Polygon
        self.associated_poly = poly if poly else random.choices([TRIANGLE, SQUARE, PENTAGON, STAR, CIRCLE],
                                                                weights=polygon_probabilities, k=1)[0]

        # Color
        self.color = self.get_a_valid_color()
        self.darker_color = (int(self.color[0] * 0.3), int(self.color[1] * 0.3), int(self.color[2] * 0.3))
        
        # Trail
        self.paint_trails = paint_trails
        self.trail_length = random.randint(min_trail_length, max_trail_length)
        self.trails = []
        
        # For collision calculations
        self.to_collide_now = set()
        self.last_collided = set()
        
    def get_current_position(self):
        return (int(self.position_vec.x), int(self.position_vec.y))

    def move(self):
        self.position_vec = self.position_vec + self.speed_mult * self.speed_vec

        # Change direction
        self.try_to_change_dir()
        self.apply_internal_rotation()

        # Collision
        self.collide_with_border()

        # Trails
        if not self.paint_trails:
            return
        if self.trails:
            if len(self.trails) == self.trail_length:
                self.trails.pop()
        self.trails.insert(0, self.position_vec)

    def generate_random_speed_vec(self):
        self.speed_vec = Vector2(random.uniform(-1, 1), random.uniform(-1, 1))

    def try_to_change_dir(self):
        if not random.random() < self.speed_rot_randomisation:
            return
        self.speed_vec.rotate()

    def apply_internal_rotation(self):
        self.internal_rotation += self.internal_rotation_increment
        if self.internal_rotation == 0:
            self.internal_rotation += 1

    def revert_internal_rotation(self):
        self.internal_rotation_increment *= -1

    def collide_with_border(self):
        x, y = self.get_current_position()

        if x <= self.radius:
            self.position_vec = Vector2(self.radius, self.position_vec.y)
            if self.speed_vec.x < 0:
                self.speed_vec = Vector2(self.speed_vec.x * -1, self.speed_vec.y)
                self.revert_internal_rotation()
        if y <= self.radius:
            self.position_vec = Vector2(self.position_vec.x, self.radius)
            if self.speed_vec.y < 0:
                self.speed_vec = Vector2(self.speed_vec.x, self.speed_vec.y * -1)
                self.revert_internal_rotation()
        if x >= self.boundaries_x:
            self.position_vec = Vector2(self.boundaries_x, self.position_vec.y)
            if self.speed_vec.x > 0:
                self.speed_vec = Vector2(self.speed_vec.x * -1, self.speed_vec.y)
                self.revert_internal_rotation()
        if y >= self.boundaries_y:
            self.position_vec = Vector2(self.position_vec.x, self.boundaries_y)
            if self.speed_vec.y > 0:
                self.speed_vec = Vector2(self.speed_vec.x, self.speed_vec.y * -1)
                self.revert_internal_rotation()

    def get_is_safe_distance_from_other(self, other):
        distance = self.position_vec.get_distance_to_other(other.position_vec)
        if distance > 2.4 * self.radius:
            return True
        return False

    def collide_with_particle(self, other):
        if self.get_is_safe_distance_from_other(other):
            return None

        # Collision type
        collision = None
        if self.collision_type == other.collision_type:
            collision = self.collision_type
            if self.collision_type == REBOUND_COLL:
                self.prepare_rebound_collision(other)

        return collision

    def prepare_rebound_collision(self, other):
        self.to_collide_now.add(other)

    def solve_rebound_collisions(self):
        collisions_list = list(self.to_collide_now)

        for l in self.last_collided:
            if l in collisions_list:
                return

        for other in collisions_list:
            other.last_collided.add(self)
            if self in other.to_collide_now:
                other.to_collide_now.remove(self)

        self.last_collided = set(collisions_list)
        collisions_list.append(self)

        speed_vec_list = deque([p.speed_vec.get_xy_tuple() for p in collisions_list])
        speed_vec_list.rotate(1)

        for i in range(len(collisions_list)):
            in_x, in_y = speed_vec_list[i]
            collisions_list[i].speed_vec = Vector2(in_x, in_y)

        self.to_collide_now.clear()

    def get_a_valid_color(self):
        total = 0
        while total < 255:
            c = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
            total = c[0] + c[1] + c[2]
        return c

    def get_associated_poly(self):
        return self.associated_poly

    def get_shape_coords(self):

        # Polygon templates
        triangle_coords = [(0, self.radius), (self.radius * 0.86, self.radius * -0.5),
                           (self.radius * -0.86, self.radius * -0.5)]

        square_coords = [(0, self.radius), (self.radius, 0), (0, -self.radius), (-self.radius, 0)]

        pentagon_coords = [(0, self.radius), (self.radius * 0.9511, self.radius * 0.3),
                           (self.radius * 0.5878, self.radius * -0.8),
                           (self.radius * -0.5878, self.radius * -0.8),
                           (self.radius * -0.9511, self.radius * 0.3)]

        star_coords = [(0, self.radius), (self.radius * 0.35, self.radius * 0.35), (self.radius, 0),
                       (self.radius * 0.35, self.radius * -0.35), (0, -self.radius),
                       (self.radius * -0.35, self.radius * -0.35), (-self.radius, 0),
                       (self.radius * -0.35, self.radius * 0.35)]

        # Returned info for circle
        if self.associated_poly == CIRCLE:
            return Vector2.get_bounding_box((Vector2(self.radius, -self.radius) + self.position_vec),
                                            (Vector2(-self.radius, self.radius)) + self.position_vec)
        # Returned info for polygons
        if self.associated_poly == TRIANGLE:
            coords = triangle_coords
        elif self.associated_poly == SQUARE:
            coords = square_coords
        elif self.associated_poly == PENTAGON:
            coords = pentagon_coords
        elif self.associated_poly == STAR:
            coords = star_coords

        vertices = [Vector2(sv[0], sv[1]) for sv in coords]

        for vertex in vertices:
            vertex.rotate(self.internal_rotation, False)

        return [(v + self.position_vec).get_xy_tuple() for v in vertices]

    def __str__(self):
        return 'Moving particle | ' \
               'Position: [{}] ' \
               'Speed factor: [{:.02f}] ' \
               'Speed vector: [{}] ' \
               'Shape: [{}]'.format(self.position_vec, self.speed_mult, self.speed_vec, self.associated_poly)
